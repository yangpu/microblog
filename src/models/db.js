const config = require('../config')

const Db = require('mongodb').Db

const Connection = require('mongodb').Connection

const Server = require('mongodb').Server

module.exports = new Db(config.db, new Server(config.host, Connection.DEFAULT_PORT, {}))