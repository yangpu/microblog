var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
  res.render('admin', {
    title: 'express'
  })
});

// 注册页面
router.get('/reg', function (req, res, next) {
  res.render('reg', {
    title: 'express'
  })
});


router.post('/reg', function (req, res, next) {
  console.log(req)
  console.log(req.body.name)
  console.log(req.body.password)
});



module.exports = {
  router
}

// 模板文件不是孤立的展示的，默认情况下所有的模板都会继承layout.ejs， <%- body %>部分才是独特的内容，其他部分是共有的，可以看做页面的框架 -->
//   <!-- <% code %> js代码
//   <%= code %> 替换过的html特殊字符串的内容
//   <% code %> 显示原始的html

//  多种模板
// res.render(index, {layout: 'admin'})
