const express = require('express')
const {router} = require('./routes')
const path = require('path')
const util = require('util')

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const config = require('./config')

var expressLayouts = require('express-ejs-layouts');
var bodyParser = require('body-parser'); // 解析客户端请求，通常是post发送的内容
// var methodOverride = require('method-override'); // 用于支持定制的http的方法，put delete





const app = module.exports = express() // 创造一个服务器的实例


// 配置
// 模板引擎
app.set('view engine', 'ejs')
app.set('views', path.resolve(__dirname + '/views')) // views



// 设置中间件
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
// app.use(methodOverride());
app.use(expressLayouts) // 关于nodejs express4.X框架不支持layout模板的问题解决
app.use(express.static(path.resolve(__dirname + '/public'))) // 静态文件的支持，静态资源的目录的名称

// 通用 logErrors 可能将请求和错误信息写入 stderr
function logErrors(err, req, res, next) {
  console.error(err.stack);
  next(err);
}

// clientErrorHandler 定义如下，错误会显式传递到下一项
function clientErrorHandler(err, req, res, next) {
  if (req.xhr) {
    res.status(500).send({ error: 'Something failed!' });
  } else {
    next(err);
  }
}
// errorHandler 函数可以如下实现
function errorHandler(err, req, res, next) {
  res.status(500);
  res.render('error', { error: err });
}

app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);


// 把会话信息存在数据库中
// app.use(session({
//   secret: config.cookieSecret,
//   store: new MongoStore({ db: config.db })
// }));


// 视图助手
// 静态
app.locals.inspect = (obj) => util.inspect(obj, true)

// 动态
app.use((req,res,next) =>{
  res.locals.headers = req.headers
  next();
});



// 路由的方法
// 路由的规则
// mount the router on the app
app.use('/', router);



// 应用程序会启动服务器，并在端口 3000 上侦听连接。
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});